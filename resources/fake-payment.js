var elServiceNo = document.getElementById('serviceNo');
var elDate = document.getElementById('paymentDate');
var elAmount = document.getElementById('amount');
var elAmount1 = document.getElementById('amount1');
var elAmount2 = document.getElementById('amount2');

function setUpDefaultData() {
    elServiceNo.value = 1234567;
    elAmount.value = 160;
    elDate.value = new Date().toLocaleString('en-GB').split(' ')[0].split('/').reverse().join('-').replace(',', '');
}

function createFakePayment() {
    var serviceNo = elServiceNo.value;
    var amount = elAmount.value;
    var date = elDate.valueAsDate;

    window.location = encodeURI('vpc_regupreceipt.php.htm?serviceNo=' + serviceNo + '&paymentDate=' + date + '&amount=' + amount);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function setUpFakePayment() {


    var serviceNo = getParameterByName('serviceNo');
    var amount = getParameterByName('amount');
    var paymentDateStr = getParameterByName('paymentDate');

    console.log("elServiceNo", elServiceNo);
    console.log("ServiceNUMMBER", serviceNo);

    elServiceNo.innerHTML = serviceNo;
    elAmount1.innerHTML = amount;
    elAmount2.innerHTML = amount;
    elDate.innerHTML = new Date(paymentDateStr).toLocaleString('en-GB').split(' ')[0].split('/').reverse().join('-').replace(',', '');
}