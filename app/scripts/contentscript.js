console.log("Loading Payir EB Data Management Chrome extension!");
window.addEventListener("load", function () {
    'use strict';
    var app = angular.module("Payir-EB", []),
        html = document.querySelector("html");

    html.setAttribute("ng-app", "Payir-EB");
    html.setAttribute("ng-csp", "");

    //angular.bootstrap(html, ["Payir-EB"]);

    app.constant('ErrorCode', {
        'CONN_REFUSED': 20,
        'SQL_DUPLICATE_ENTRY': 1062,
        'SQL_MISSING_CUSTOMER': 1452
    });

    app.controller("EBCtrl", ['$scope', '$timeout', '$filter', '$http', 'ErrorCode', function ($scope, $timeout, $filter, $http, ErrorCode) {

        //1. Obtain a reference to the div containing the content table
        //2. Obtain a reference to the table containing all the content
        //// The table at index 0 shows the title of the table (wtf?)
        //3. The table at index 1 is the relevant table
        var displayContent = document.getElementById("display_content"),
            contentTable = displayContent.getElementsByTagName('table')[1],
            allTds = contentTable.getElementsByTagName('td');


        console.log("Service number = ", allTds[1].innerHTML);
        console.log("Name = ", allTds[4].innerHTML);
        console.log("Bill Amount = ", allTds[6].innerHTML);
        console.log("Bill Month = ", allTds[9].innerHTML);
        console.log("Bill Payment date = ", allTds[14].innerHTML);
        console.log("Bill Payment date = ", $filter("date")(Date.parse(allTds[14].innerHTML), 'yyyy-MM-dd'));

        //Original
        var billPayment = {
            "serviceNo": allTds[1].innerHTML,
            "amount": allTds[6].innerHTML,
            "paymentDate": $filter("date")(Date.parse(allTds[14].innerHTML), 'yyyy-MM-dd')
        };

        // Hardcoded serviceNo and random amount
        //        var billPayment = {
        //            "serviceNo": "234567",
        //            "amount": allTds[6].innerHTML + Math.random() * 100 + 2,
        //            "paymentDate": $filter("date")(Date.parse(allTds[14].innerHTML), 'yyyy-MM-dd')
        //        };

        //Hardcoded serviceNo
        //        var billPayment = {
        //            "serviceNo": "234567",
        //            "amount": allTds[6].innerHTML,
        //            "paymentDate": $filter("date")(Date.parse(allTds[14].innerHTML), 'yyyy-MM-dd')
        //        };

        //TODO Fallback port address

        $scope.sendPayment = function () {
            $scope.statusText = "Loading";
            $scope.status = 0;

            $http({
                method: 'POST',
                url: 'http://localhost:5555/billPayment',
                data: billPayment,
                processData: false,
                responseType: "json",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (succ) {
                console.log("Payment info saved");
                $scope.statusText = "Payment saved successfully";
                $scope.status = 1;
                $scope.showRetry = false;
            }, function (err) {
                $scope.showRetry = false;
                console.log("Error while saving payment info", err);
                if (err.status === 0) {
                    $scope.statusText = "Unable to connect to Payir EB Data Management app. Please make sure it is running and click Retry";
                    $scope.showRetry = true;
                    $scope.status = -1;
                } else if (err && err.data && err.data.custom) {
                    // Custom (non-SQL) error handling
                    switch (err.data.errNo) {
                    case ErrorCode.CONN_REFUSED:
                        $scope.statusText = "Unable to connect to the database. Please make sure that MySQL is running and click Retry";
                        $scope.showRetry = true;
                        $scope.status = -1;
                        break;

                    default:
                        $scope.statusText = "Unknown custom error occurred!";
                        $scope.status = -1;
                    }
                } else if (err && err.data && !err.data.custom) {
                    // SQL error handling
                    switch (err.data.errNo) {
                        //Duplicate entry
                    case ErrorCode.SQL_DUPLICATE_ENTRY:
                        $scope.statusText = "This payment information has already been saved!";
                        $scope.status = 0;
                        break;

                    case ErrorCode.SQL_MISSING_CUSTOMER:
                        $scope.statusText = allTds[4].innerHTML + " is not a known customer. Please provide customer details in the app and click Retry";
                        $scope.status = -1;
                        $scope.showRetry = true;
                        break;

                    default:
                        $scope.statusText = "Unknown error occurred!";
                        $scope.status = -1;
                    }
                }
            });
        };

        $scope.sendPayment();
    }]);

    var mainContent = document.getElementById("j_idt98_content"),
        displayContent = document.getElementById("display_content");

    if (!displayContent) {
        return;
    }

    var contentTable = displayContent.getElementsByTagName('table')[1],
        statusBox = document.createElement("div"),
        payirIcon = document.createElement("img"),
        statusText = document.createElement("p"),
        retryBtn = document.createElement("button");

    displayContent.setAttribute("ng-controller", "EBCtrl");

    payirIcon.setAttribute("src", chrome.extension.getURL("images/icon-128.png"));
    payirIcon.setAttribute("class", "payir-icon");

    statusText.setAttribute("class", "payir-text");
    statusText.setAttribute("ng-class", "{'message-loading': status === 0, 'message-success': status === 1, 'message-failure': status === -1}");
    statusText.innerHTML = "{{statusText}}";

    retryBtn.setAttribute("ng-click", "sendPayment()");
    retryBtn.setAttribute("ng-show", "showRetry");
    retryBtn.setAttribute("class", "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left ui-button-text retry-btn");
    retryBtn.innerHTML = "Retry";

    statusBox.appendChild(payirIcon);
    statusBox.appendChild(statusText);
    statusBox.appendChild(retryBtn);

    displayContent.insertBefore(statusBox, contentTable.getElementsByTagName('table')[0]);
});